package com.devcamp.task58s20.drinkapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58s20.drinkapi.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
    
}
